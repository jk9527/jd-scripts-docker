#!/bin/bash

p_name=node

while true
do
	#every 3s check
	pid="$(ps -ef|grep $p_name|awk '{print $2}'|head -n1)"
	echo $pid
	ptime="$(ps -eo pid,etime|grep $pid|awk '{print $2}' |head -n1)"
	echo $ptime
	pstatus="$(echo $ptime|awk '{split($1,tab,/:/); if (length(tab)>=3) {print 1}else{print 0} }')"
	echo $pstatus
	#if time > 1 hour,return 0,else 1
	if [ $pstatus = "1" ];then
		kill -9 $pid
		echo "kill success " $pid $p_name
		sleep 3
		continue
	else
		echo "没有超过1小时的占用进程"
		break;
	fi
done
