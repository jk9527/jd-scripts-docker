#/bin/bash

name="$(docker ps -a|grep jd |awk '{print $12}')"
echo $name
namearr=($name)

if [ $1 ]; then
  
  echo $1
  if [ $1 == "sync" ];then
	  echo "更新最新代码"
	  for i in "${!namearr[@]}";do
	     echo "$i==========================>${namearr[i]}"
	     echo "$(docker exec ${namearr[i]} bash  '/sync')"
	     sleep 2
	  done
  elif [ $(echo $1 | grep "\.js") ];then
	  echo "执行脚本：$1"
	  for i in "${!namearr[@]}";do
	     echo "$i==========================>${namearr[i]}"
	     echo "$(docker exec ${namearr[i]} bash -c 'set -o allexport; source /all; source /env; cd /scripts; node '$1'')"
	     sleep 2
	  done
  else
	  echo "参数错误"
  fi
else
  echo "请输入要执行的脚本名称！"
fi
